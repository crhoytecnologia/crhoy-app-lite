module.exports = function (grunt) {
    grunt.loadNpmTasks("grunt-includes");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-compass");
    grunt.loadNpmTasks("grunt-contrib-concat");
    //grunt.loadNpmTasks("grunt-contrib-uglify");

    // https://github.com/mishoo/UglifyJS2/tree/harmony
    grunt.registerTask("minifyJS", "", function () {
        var fs = require("fs");
        var UglifyJS = require("uglify-es");

        try {
            var router = UglifyJS.minify(fs.readFileSync("views/js/config/routes.router.js", "utf8"), {
                compress: false
            });
            var config = UglifyJS.minify([
                fs.readFileSync("views/js/config/categories.static.js", "utf8"),
                fs.readFileSync("views/js/config/socialNetwork.static.js", "utf8"),
                fs.readFileSync("views/js/config/es-language-calendar.static.js", "utf8")
            ], {
                compress: false
            });
            var directives = UglifyJS.minify([
                fs.readFileSync("views/js/directives/accordion.directive.js", "utf8"),
                fs.readFileSync("views/js/directives/iosBackButton.directive.js", "utf8"),
                fs.readFileSync("views/js/directives/tabset.directive.js", "utf8"),
                fs.readFileSync("views/js/directives/title.directive.js", "utf8")
            ], {
                compress: false
            });
            var filters = UglifyJS.minify([
                fs.readFileSync("views/js/filters/html.filter.js", "utf8"),
                fs.readFileSync("views/js/filters/icdn.filter.js", "utf8")
            ], {
                compress: false
            });
            var services = UglifyJS.minify([
                fs.readFileSync("views/js/services/autor.service.js", "utf8"),
                fs.readFileSync("views/js/services/boletin.service.js", "utf8"),
                fs.readFileSync("views/js/services/buscador.service.js", "utf8"),
                fs.readFileSync("views/js/services/categorias.service.js", "utf8"),
                fs.readFileSync("views/js/services/comentarios.service.js", "utf8"),
                fs.readFileSync("views/js/services/especiales.service.js", "utf8"),
                fs.readFileSync("views/js/services/etiqueta.service.js", "utf8"),
                fs.readFileSync("views/js/services/home.service.js", "utf8"),
                fs.readFileSync("views/js/services/masLeidas.service.js", "utf8"),
                fs.readFileSync("views/js/services/noticia.service.js", "utf8"),
                fs.readFileSync("views/js/services/samba.service.js", "utf8"),
                fs.readFileSync("views/js/services/ultimas.service.js", "utf8")
            ], {
                compress: false
            });
            var app_component = UglifyJS.minify(fs.readFileSync("views/js/components/app.component.js", "utf8"), {
                compress: false
            });
            var components = UglifyJS.minify([
                fs.readFileSync("views/js/components/autor.component.js", "utf8"),
                fs.readFileSync("views/js/components/boletin.component.js", "utf8"),
                fs.readFileSync("views/js/components/buscador.component.js", "utf8"),
                fs.readFileSync("views/js/components/category.component.js", "utf8"),
                fs.readFileSync("views/js/components/especial.component.js", "utf8"),
                fs.readFileSync("views/js/components/especiales.component.js", "utf8"),
                fs.readFileSync("views/js/components/home.component.js", "utf8"),
                fs.readFileSync("views/js/components/masLeidas.component.js", "utf8"),
                fs.readFileSync("views/js/components/noticia.component.js", "utf8"),
                fs.readFileSync("views/js/components/tema.component.js", "utf8"),
                fs.readFileSync("views/js/components/ultimas.component.js", "utf8")
            ], {
                compress: false
            });

            if (router.error || app_component.error || config.error || directives.error || filters.error || services.error || components.error) {
                throw router.error || app_component.error || config.error || directives.error || filters.error || services.error || components.error;
            } else {
                fs.writeFileSync("temp/js_min/app.component.js", app_component.code, "utf8");
                fs.writeFileSync("temp/js_min/router.min.js", router.code, "utf8");
                fs.writeFileSync("temp/js_min/config.min.js", config.code, "utf8");
                fs.writeFileSync("temp/js_min/directives.min.js", directives.code, "utf8");
                fs.writeFileSync("temp/js_min/filters.min.js", filters.code, "utf8");
                fs.writeFileSync("temp/js_min/services.min.js", services.code, "utf8");
                fs.writeFileSync("temp/js_min/components.min.js", components.code, "utf8");
            }
        } catch (ex) {
            console.error(ex);
        }
    });


    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        compass: {
            dist: {
                options: {
                    sassDir: "views/sass/",
                    cssDir: "temp/css",
                    outputStyle: "compressed",
                    trace: true
                }
            }
        },
        includes: {
            dist: {
                files: [{
                        src: ["pages/*"],
                        flatten: true,
                        dest: "www/",
                        cwd: "."
                    }]
            }
        },
        copy: {
            img: {
                expand: true,
                cwd: "views/img/",
                src: "**",
                dest: "www/img/",
                flatten: true
            },
            js: {
                expand: true,
                cwd: "temp/js_min/",
                src: "**",
                dest: "www/js/",
                flatten: true
            },
            css_libs_fonts: {
                files: [
                    {
                        expand: true,
                        cwd: "views/libs/onsenUI/css/font_awesome/",
                        src: "**/*",
                        dest: "www/css/font_awesome/"
                    },
                    {
                        expand: true,
                        cwd: "views/libs/onsenUI/css/ionicons/",
                        src: "**/*",
                        dest: "www/css/ionicons/"
                    },
                    {
                        expand: true,
                        cwd: "views/libs/onsenUI/css/material-design-iconic-font/",
                        src: "**/*",
                        dest: "www/css/material-design-iconic-font/"
                    }
                ]
            },
            components_cordova: {
                expand: true,
                cwd: "views/components/",
                src: "**",
                dest: "www/components/",
                flatten: true
            }
        },
        concat: {
            options: {
                separator: "\n"
            },
            css_components: {
                src: [
                    "temp/css/components/*.css"
                ],
                dest: "temp/css_min/components.min.css"
            },
            css_pages: {
                src: [
                    "temp/css/pages/*.css"
                ],
                dest: "temp/css_min/pages.min.css"
            },
            css_core: {
                src: [
                    "temp/css/*.css",
                    "temp/css_min/*.min.css"
                ],
                dest: "www/css/core.min.css"
            },
            css_libs: {
                src: [
                    "views/libs/onsenUI/css/onsenui.min.css",
                    "views/libs/onsenUI/css/onsen-css-components.min.css"
                ],
                dest: "www/css/libs.min.css"
            },
            js_libs: {
                src: [
                    "views/libs/vue.min.js",
                    "views/libs/vue-resource.min.js",
                    "views/libs/vue-resource.min.js",
                    "views/libs/vue-router.min.js",
                    "views/libs/vuejs-datepicker.min.js",
                    "views/libs/onsenUI/js/onsenui.min.js",
                    "views/libs/vue-onsenui.min.js"
                ],
                dest: "www/libs/libs.min.js"
            },
            js_router: {
                src: "temp/js_min/router.min.js",
                dest: "www/js/router.min.js"
            },
            js_app_component: {
                src: "temp/js_min/app.component.js",
                dest: "www/js/app.component.min.js"
            }
        }
    });
    grunt.registerTask("default", [
        "compass", "minifyJS", "copy", "includes", "concat"
    ]);
};