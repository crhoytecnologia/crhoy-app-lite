const _dbFirebase = {
    srcApp: "https://www.gstatic.com/firebasejs/5.4.1/firebase-app.js",
    srcFirestore: "https://www.gstatic.com/firebasejs/5.5.3/firebase-firestore.js",
    db: null,
    intervalID: null,
    interval: 10000,
    config: {
        apiKey: "AIzaSyDJ4O2XjrloaS61ji1BeoKWd27zhP1PYIg",
        authDomain: "crhoy-lite.firebaseapp.com",
        databaseURL: "https://crhoy-lite.firebaseio.com",
        projectId: "crhoy-lite",
        storageBucket: "crhoy-lite.appspot.com",
        messagingSenderId: "290658770822"
    },
    pendingDocuments: [],
    init: function () {
        var head = document.getElementsByTagName("head")[0];
        var scriptApp = document.createElement("script");
        scriptApp.async = true;
        scriptApp.src = this.srcApp;
        scriptApp.type = "text/javascript";

        var self = this;
        scriptApp.onload = function () {
            var scriptFireStore = document.createElement("script");
            scriptFireStore.async = true;
            scriptFireStore.src = self.srcFirestore;
            scriptFireStore.type = "text/javascript";
            scriptFireStore.onload = function () {
                firebase.initializeApp(self.config);
                self.db = firebase.firestore();
                // INICIAR UN INTERVALO
                self.intervalID = setInterval(function () {
                    self.proccessPendingDocuments();
                }, self.interval);
            };
            head.appendChild(scriptFireStore);
        };
        head.appendChild(scriptApp);
    },
    proccessPendingDocuments: function () {
        if (this.pendingDocuments.length > 0) {
            this.pendingDocuments.forEach(function (pendingDocument, index, array) {
                this.db.collection(pendingDocument.collection).add(pendingDocument.data).then(function (docRef) {
                    // BORRAR DOCUMENTO
                    array.splice(index, 1);
                }).catch(function (error) {
                    console.error("Error agregando el  documento");
                    console.error(error);
                });
            }, this);
        }
    },
    addDocument: function (collection, data) {
        this.pendingDocuments.push({
            collection: collection,
            data: data
        });
    }
};
_dbFirebase.init();