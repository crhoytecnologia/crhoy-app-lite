Vue.use(VueResource);         // HAY QUE DECIRLE A VUE QUE UTILICE ESTE PLUGIN PARA LOS REQUEST
Vue.use(VueRouter);           // HAY QUE DECIRLE A VUE QUE UTILICE ESTE PLUGIN PARA EL ROUTING 

var app_crhoy = new Vue({
      el: "#app",
      data: {
            currentPage: "home",
            openSide: false,
            pages: [{
                  title: "Inicio",
                  route: "/"
            }, {
                  title: "Últimas",
                  route: "/ultimas"
            }, {
                  title: "Más Leídas",
                  route: "/masLeidas"
            }, {
                  title: "Especiales",
                  route: "/especiales"
            }],
            socialNetworks: _socialNetworks,
            categories: _categories,
            openSide: false,
            deviceReady: false,
            nightMode: false
      },
      computed: {
            _isIOS: function () {
                  return this.$ons.platform.isIPhone() || this.$ons.platform.isIPhoneX() || this.$ons.platform.isIPad() || this.$ons.platform.isIOS7above() || this.$ons.platform.isIOSSafari() || this.$ons.platform.isSafari() || this.$ons.platform.isIOS();
            }
      },
      router: _router,
      created: function () {
            this.$router.replace("/");
      },
      methods: {
            navToCategory: function (category) {
                  this.$router.push({
                        name: "category",
                        params: category
                  });
                  this.openSide = false;
            },
            navToSearch: function () {
                  this.$router.push({
                        name: "buscador"
                  });
                  this.openSide = false;
            },
            back: function (ev) {
                  // this.currentUrl.hash !== "#/"
                  switch (this.$route.name) {
                        case "home":
                              // https://onsen.io/v2/api/vue/$ons.notification.html#method-confirm
                              this.$ons.notification.confirm("¿Quiére salir del app?", {
                                    buttonLabels: ["Cancelar", "Ok"],
                                    title: "CRHoy"
                              }).then(function (response) {
                                    if (response == 1) {
                                          // https://stackoverflow.com/questions/12297525/exit-from-app-when-click-button-in-android-phonegap
                                          navigator.app.exitApp();
                                    }
                              });
                              break;
                        case "indefinido":
                              this.$router.go(-2);
                              break;
                        default:
                              window.history.back();
                              break;
                  }
            },
            changeMode: function () {
                  var styleEl = document.getElementById("styleMode");
                  if (this.nightMode) {
                        styleEl.href = "libs/onsenUI/css/onsen-css-components.min.css";
                        this.nightMode = false;
                  } else {
                        styleEl.href = "libs/onsenUI/css/dark-onsen-css-components.min.css";
                        this.nightMode = true;
                  }
            }
      },
      mounted: function () {
            var self = this;
            this.$ons.ready(function () {
                  self.deviceReady = true;

                  /* DECIRLE A ONSEN QUE NOS DE EL CONTROL DEL EVENTO DEL BOTON DE BACK DE ANDROID
                   *  REFERENCIA: https://onsen.io/v2/api/vue/$ons.html#method-setDefaultDeviceBackButtonListener
                   */
                  self.$ons.setDefaultDeviceBackButtonListener(self.back);
                  // CONFIGURAR EL PLUGIN DE FIREBASE PARA NOTIFICACIONES
                  window.FirebasePlugin.setAnalyticsCollectionEnabled(true);
                  window.FirebasePlugin.setScreenName("portada");
                  window.FirebasePlugin.getToken(function (token) {
                        // save this server-side and use it to push notifications to this device
                        window.localStorage.setItem("firebase_token", token);
                  }, function (error) {
                        _dbFirebase.addDocument("errors", {
                              error: error,
                              date: new Date(),
                              customError: "window.FirebasePlugin.getToken"
                        });
                  });
                  window.FirebasePlugin.onTokenRefresh(function (token) {
                        // save this server-side and use it to push notifications to this device
                        window.localStorage.setItem("firebase_token", token);
                  }, function (error) {
                        _dbFirebase.addDocument("errors", {
                              error: error,
                              date: new Date(),
                              customError: "window.FirebasePlugin.onTokenRefresh"
                        });
                  });

                  var callback = function () {
                        // REVISAR SI TENEMOS PERMISO PARA ENVIAR NOTIFICACIONES
                        window.FirebasePlugin.hasPermission(function (data) {
                              if (data.isEnabled) {
                                    // SUBSCRIBIR AL TEMA POR DEFECTO
                                    window.FirebasePlugin.subscribe("noticias_importantes");

                                    window.FirebasePlugin.onNotificationOpen(function (notification) {
                                          /* CUANDO EL APP NO ESTA ABIERTA, DEBEMOS CORROBORAR SI LA NOTIFICACION HA SIDO "tapeada", SINO,
                                           * QUIERE DECIR QUE LA NOTIFICACION HA SIDO RECIBIDA CON EL APP ABIERTA
                                           */
                                          if (notification.tap === true) {
                                                self.$router.push({
                                                      name: "noticia",
                                                      params: {
                                                            noticia_url: notification.url,
                                                            noticia_id: "null"
                                                      }
                                                });
                                                _dbFirebase.addDocument("newsOpenByNotification", {
                                                      url: notification.url,
                                                      title: notification.title,
                                                      date: new Date(),
                                                      inside: false
                                                });
                                                /*     JSON RECIBIDO
                                                      {
                                                            "google.delivered_priority": "high",
                                                            "google.sent_time": 1538516005744,
                                                            "google.ttl": 3600,
                                                            "google.original_priority": "high",
                                                            "img": "https://cdn.crhoy.net/imagenes/2017/05/tecnologia2.jpg",
                                                            "tap": true,
                                                            "url": "https://www.crhoy.com/tecnologia/resumen-de-conferencias-e3-2018-nintendo/",
                                                            "from": "/topics/nacionales",
                                                            "title": "Resumen de conferencias E3 2018: Nintendo",
                                                            "google.message_id": "0:1538516006099236%43c196c443c196c4",
                                                            "collapse_key": "com.crhoy.testpush"
                                                      }
                                                */
                                          } else {
                                                self.$ons.notification.confirm({
                                                      buttonLabels: ["Cancelar", "Ver"],
                                                      title: "CRHoy recomienda",
                                                      class: "notification_inside_app",
                                                      messageHTML: "<ul class='list list--noborder'>"
                                                            + "<li class='list-item list-item--nodivider'>"
                                                            + "<div class='list-item__left'>"
                                                            + "<img class='list-item__thumbnail' src='" + notification.img + "' alt='" + notification.title + "'>"
                                                            + "</div>"
                                                            + "<div class='list-item__center'>"
                                                            + "<div class='list-item__title'>" + notification.title + "</div>"
                                                            + "</div>"
                                                            + "</li>"
                                                            + "</ul>"
                                                }).then(function (response) {
                                                      if (response == 1) {
                                                            // NAVEGAR
                                                            self.$router.push({
                                                                  name: "noticia",
                                                                  params: {
                                                                        noticia_url: notification.url,
                                                                        noticia_id: "null"
                                                                  }
                                                            });
                                                            _dbFirebase.addDocument("newsOpenByNotification", {
                                                                  url: notification.url,
                                                                  title: notification.title,
                                                                  date: new Date(),
                                                                  inside: true
                                                            });
                                                      }
                                                });
                                          }
                                    }, function (error) {
                                          _dbFirebase.addDocument("errors", {
                                                error: error,
                                                date: new Date(),
                                                customError: "window.FirebasePlugin.onNotificationOpen"
                                          });
                                    });
                              }
                        }, function (error) {
                              _dbFirebase.addDocument("errors", {
                                    error: error,
                                    date: new Date(),
                                    customError: "window.FirebasePlugin.hasPermission"
                              });
                        });
                  };

                  // PEDIR PERMISO CUANDO SEA IOS
                  if (self._isIOS === true) {
                        window.FirebasePlugin.grantPermission(function () {
                              callback();
                        }, function (error) {
                              _dbFirebase.addDocument("errors", {
                                    error: error,
                                    date: new Date(),
                                    customError: "window.FirebasePlugin.grantPermission"
                              });
                        });
                  } else {
                        callback();
                  }
            });
      }
});
/*
 * https://medium.com/@felipepucinelli/how-to-add-push-notifications-in-your-cordova-application-using-firebase-69fac067e821
 * https://github.com/arnesson/cordova-plugin-firebase
 * https://github.com/phonegap/phonegap-plugin-push/blob/master/docs/API.md#pushnotificationhaspermissionsuccesshandler
 * 
 * https://console.firebase.google.com/u/0/project/test-push-178e6/notification
 * https://firebase.google.com/docs/cloud-messaging/admin/send-messages?authuser=0
 * https://medium.com/@felipepucinelli/how-to-add-push-notifications-in-your-cordova-application-using-firebase-69fac067e821
 * https://github.com/arnesson/cordova-plugin-firebase/blob/master/docs/API.md
 * https://github.com/fechanique/cordova-plugin-fcm/issues/215
 * 
 * https://docs.monaca.io/en/tutorials/firebase/
 */