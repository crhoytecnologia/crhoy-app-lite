var _especialesComponent = {
      template: "#especiales",
      mixins: [_especialesService], // DEPENDENCIAS DE SERVICIOS
      data: function () {
            return {
                  especiales: []
            };
      },
      mounted: function () {
            this.getSpecials();
      },
      methods: {
            getSpecials: function () {
                  this.especialesService().then(function (response) {
                        this.especiales = response.data.especiales;
                  }, function (error) {
                        this.errorLoadingPage = true;

                        _dbFirebase.addDocument("errors", {
                              error: error,
                              date: new Date(),
                              customError: "especials component: getSpecials"
                        });
                  });
            },
            navToSpecial: function (especial) {
                  this.$router.push({
                        name: "especial",
                        params: especial
                  })
            }
      }
};