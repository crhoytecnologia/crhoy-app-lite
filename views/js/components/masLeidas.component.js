var _masLeidasComponent = {
      template: "#masLeidas",
      mixins: [_masLeidasService], // DEPENDENCIAS DE SERVICIOS
      data: function () {
            return {
                  state: "initial",
                  noticias: []
            };
      },
      mounted: function () {
            this.getNews();
      },
      methods: {
            getNews: function (done) {
                  this.masLeidasService().then(function (response) {
                        this.noticias = response.data.masLeidas;
                        if (done) {
                              done();
                        }
                  }, function (error) {
                        this.errorLoadingPage = true;
                        if (done) {
                              done();
                        }

                        _dbFirebase.addDocument("errors", {
                              error: error,
                              date: new Date(),
                              customError: "masLeidas component: getNews"
                        });
                  });
            }
      }
};